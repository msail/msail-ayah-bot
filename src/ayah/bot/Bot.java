
package ayah.bot;

import static ayah.bot.GameBox.BACKGROUND_COLOR;
import static ayah.bot.GameBox.BIN_ONE_X_OFFSET;
import static ayah.bot.GameBox.BIN_THREE_X_OFFSET;
import static ayah.bot.GameBox.BIN_TWO_X_OFFSET;
import static ayah.bot.GameBox.BIN_Y_OFFSET;
import static ayah.bot.GameBox.START_BUTTON_X_OFFSET;
import static ayah.bot.GameBox.START_BUTTON_Y_OFFSET;
import ayah.bot.Utility;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import ayah.bot.Instructions.Colors;
import ayah.bot.Instructions.Shapes;
import ayah.bot.templatematching.Ball;
import ayah.bot.templatematching.Item;
import ayah.bot.templatematching.Templating;


public class Bot {

	//Used to extract image data and simulate mouse input from the user.
	private Robot r;
	
	//Used to store the dimensions of the instance we're playing.
	private GameBox gameBox;

	//Error range for color matching
	private final int ERROR_RANGE = 5;

	/************************************************************************/

	public static void main(String[] args) {
		// Create a bot and start it
		new Bot().start();
	}

	public void start() {
		try {
			// Instantiate the java Robot
			r = new Robot();

			// Create a game box to determine and contain the play area dimensions and constants
			gameBox = new GameBox();

			// Click the start button
			clickStart();

			// Add Key Listener
			initExitKeyListener();

			// Start the bot
			// Keep playing instances until someone manually tells the bot to stop.
			while(true)
				playInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Creates a key listener that allows user to exit bot with CTRL+C
	//TODO: Is it supposed to be CTRL+C or X? I don't care which, just make sure we know.
	private void initExitKeyListener() {
		GlobalScreen.getInstance().addNativeKeyListener(
				new NativeKeyListener() {
					@Override
					public void keyPressed(NativeKeyEvent event) {
						if (event.getKeyCode() == NativeKeyEvent.VK_X) {
							System.out.println("X was pressed. Exiting");
							System.exit(0);
						}
					}

					@Override
					public void keyReleased(NativeKeyEvent arg0) {
					}
				});
	}

	/************************************************************************/

	//Plays *ONE* instance of the game to completion.
	public void playInstance() {

		//Says whether or not the game thinks its over
		//	(ie: the background color of the startup menu is visible in the play area).
		boolean complete = (r.getPixelColor(gameBox.getLeftEdge() + 10,
				gameBox.getBottomEdge() - 10).equals(BACKGROUND_COLOR));
		
		if(complete) //Would mean we never actually started the instance for whatever reason.
		{
			clickSubmit();
			return;
		}
		
		// The parse the game instructions.
		Instructions instructions = determineInstructions();
		if(instructions.getBinColor() == null || instructions.getShapeColor() == null)
		{
			System.out.println("ERROR: Unable to parse colors!");
			complete = true;
		}
		// Convert shape color to RGB value
		Color shapeColor = Utility.convertShapeColorToRGB(instructions.getShapeColor());
		
		while(!complete)
		{
			// Find which bin to drag shapes to.
			int bin = findBin(instructions.getBinColor());
	
			System.out.println("Looking for objects.");
			// Find the object until one cannot be found.
			findObject(shapeColor, bin);
	
			// Give the game some time to load its frame.
			try { Thread.sleep(1000); } catch(InterruptedException e) {}; //Shouldn't happen.
			
			// Check if the background is the original background,
			// indicating that the game has finished.
			complete = (r.getPixelColor(gameBox.getLeftEdge() + 10,
					gameBox.getTopEdge() + 50).equals(BACKGROUND_COLOR));
			
		}
		clickSubmit();
	}

	/************************************************************************/

	//Determines which shape to find and which bin to drag shapes to
	//TODO: HEAVY WORK ZONE: We need to find a better way to decipher the instructions.
	private Instructions determineInstructions() {
		Shapes shape = Instructions.Shapes.CIRCLE;
		Instructions.Colors shapeColor = Instructions.Colors.BLUE;
		Instructions.Colors binColor = Instructions.Colors.BLUE;
		
		shapeColor = Utility.determineColor(r, gameBox, gameBox.getLeftEdge());
		if (shapeColor == null) {
			System.out.println("No color was found.");
			clickSubmit();
		}

		binColor = Utility.determineColor(r, gameBox, 200);
		if (shapeColor == null) {
			System.out.println("No color was found.");
			clickSubmit();
		}
		
		//CIRCLE, SQUARE, TRIANGE, STAR;
		System.out.println("SHAPE: " + shape);
		System.out.println("OBJ COLOR: " + shapeColor);
		System.out.println("BIN COLOR: " + binColor);
		
		return new Instructions(shape, shapeColor, binColor);
	}

	/************************************************************************/

	//Finds which bin corresponds to the given color
	private int findBin(Colors binColor) {

		Color c = Utility.convertBinColorToRGB(binColor);

		if (inRange(c, r.getPixelColor(
				gameBox.getLeftEdge() + BIN_ONE_X_OFFSET, gameBox.getTopEdge()
						+ BIN_Y_OFFSET)))
			return 1;
		else if (inRange(c, r.getPixelColor(gameBox.getLeftEdge()
				+ BIN_TWO_X_OFFSET, gameBox.getTopEdge() + BIN_Y_OFFSET)))
			return 2;
		else if (inRange(c, r.getPixelColor(gameBox.getLeftEdge()
				+ BIN_THREE_X_OFFSET, gameBox.getTopEdge() + BIN_Y_OFFSET)))
			return 3;

		// If we got this far, something went wrong. Print out the colors
		//TODO: Uncomment this when we're done messing with instruction deciphering.
		/*
		System.out.println("ERROR: Bin not found. Color debug information:");
		System.out.println(c);
		System.out.println(r.getPixelColor(gameBox.getLeftEdge()
				+ BIN_ONE_X_OFFSET, gameBox.getTopEdge() + BIN_Y_OFFSET));
		System.out.println(r.getPixelColor(gameBox.getLeftEdge()
				+ BIN_TWO_X_OFFSET, gameBox.getTopEdge() + BIN_Y_OFFSET));
		System.out.println(r.getPixelColor(gameBox.getLeftEdge()
				+ BIN_THREE_X_OFFSET, gameBox.getTopEdge() + BIN_Y_OFFSET));*/
		return 0;
	}

	/************************************************************************/

	//Calculates whether the two colors are equal within an acceptable margin of error.
	private boolean inRange(Color c1, Color c2) {
		if (c2.getRed() - ERROR_RANGE < c1.getRed()
				&& c2.getRed() < c2.getRed() + ERROR_RANGE) {
			if (c2.getBlue() - ERROR_RANGE < c1.getBlue()
					&& c2.getBlue() < c2.getBlue() + ERROR_RANGE) {
				if (c2.getGreen() - ERROR_RANGE < c1.getGreen()
						&& c2.getGreen() < c2.getGreen() + ERROR_RANGE) {
					return true;
				}
			}

		}

		return false;
	}

	/************************************************************************/

	/**
	 * Searches objects matching the instructions and moves them to the proper bin.
	 */
	//TODO: Consider renaming this method to something more appropriate.
	//TODO: Make the template matcher work with all types of objects.
	private void findObject(Color shapeColor, int bin) {
		
		try {
			BufferedImage img;
			//We instantiate the template matcher with the type of item to find. In this demo case, a 'blue_star' object.
			Templating matcher = new Templating(ImageIO.read(new File("object_images/blue_star.png")), new Ball(new double[] {0,0}));
			
			//This will be a list of all objects found that match the template.
			ArrayList<Item> items;
			
			//Will hold the position of the object relative to the top left corner of the image we send to the matcher.
			double[] pos;
			//The int-converted values added to the gameBox left and top bounds so that they reflect screen coordinates.
			int[] adjustedPos;
			
			do
			{
				img = r.createScreenCapture(new Rectangle(gameBox.getLeftEdge(), gameBox.getTopEdge(),
						gameBox.getRightEdge() - gameBox.getLeftEdge(), gameBox.getBottomEdge() - gameBox.getTopEdge()));
				
				items = matcher.findItems(img); //Does the real work.
				if(items.size() == 0) //Prevent errors if no objects found.
					return;
				
				pos = items.get(0).getImgPos(); //Just use the first object, we'll rescan after each object, it's cheap.
				adjustedPos = new int[] { (int)pos[0]+gameBox.getLeftEdge(), (int)pos[1]+gameBox.getTopEdge() };
				
				// Move the mouse to this position and click on the shape
				r.mouseMove(adjustedPos[0], adjustedPos[1]);
				try {
					Thread.sleep(50);
					r.mousePress(InputEvent.BUTTON1_MASK);
					System.out.println("Grabbed object.");
					Thread.sleep(50);
				} catch(InterruptedException e) { e.printStackTrace(); }; //Shouldn't happen.

				moveToBin(bin, adjustedPos[0], adjustedPos[1]);
			}while(items.size() > 0);
		} catch (IOException e) //Shouldn't happen.
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	/************************************************************************/

	//TODO: We should move the objects further down into the bin, sometimes they don't register.
	//Moves whatever shape is selected to chosen bin
	private void moveToBin(int bin, int i, int j) {
		try {
			// Move the mouse to this position and click on the shape
			r.mouseMove(i, j);
			Thread.sleep(50);
			r.mousePress(InputEvent.BUTTON1_MASK);
			System.out.println("Got One!");
			Thread.sleep(50);
	
			int x = 0;
			switch (bin) {
			case 0:
				System.out.println("ERROR: The bin color could not be found.");
				clickSubmit(); //Recover by ending the current instance of the game.
				return;
			case 1:
				x = BIN_ONE_X_OFFSET;
				break;
			case 2:
				x = BIN_TWO_X_OFFSET;
				break;
			case 3:
				x = BIN_THREE_X_OFFSET;
				break;
			}
	
			moveMouse(gameBox.getLeftEdge() + x, gameBox.getTopEdge() + BIN_Y_OFFSET, i, j);
			Thread.sleep(100);
			r.mouseRelease(InputEvent.BUTTON1_MASK);
		} catch(InterruptedException e) { e.printStackTrace(); }
	}

	/************************************************************************/

	//Moves the mouse to the specified x and y using human-like motion
	private void moveMouse(int x, int y, int i, int j) {
		try {
			int n = 10; //How many waypoints to go through on the way to our destination
	
			// Move mouse to bin in n moves
			for (int k = 0; k < n; k++) {
				r.mouseMove(i + k * (x - i) / n, j);
				Thread.sleep(50);
			}
	
			// Move mouse to bin in n moves
			for (int k = 0; k < n; k++) {
				r.mouseMove(x, j + k * (y - j) / n);
				Thread.sleep(50);
	        }
		} catch(InterruptedException e) { e.printStackTrace(); } //Shouldn't ever happen.
	}

	/************************************************************************/

	//Click the start button
	private void clickStart() {
		try
		{
			r.mouseMove(gameBox.getLeftEdge() + START_BUTTON_X_OFFSET,
					gameBox.getTopEdge() + START_BUTTON_Y_OFFSET);
			Thread.sleep(1000);
			r.mousePress(InputEvent.BUTTON1_MASK);
			Thread.sleep(1000);
			r.mouseRelease(InputEvent.BUTTON1_MASK);
			Thread.sleep(1000);
		} catch (InterruptedException e) { e.printStackTrace(); } //Shouldn't happen.
	}

	/************************************************************************/

	//Click the submit button
	private void clickSubmit() {
		try {
			r.mouseMove(gameBox.getLeftEdge() + 35, gameBox.getTopEdge() + 185);
			Thread.sleep(3);
			r.mousePress(InputEvent.BUTTON1_MASK);
			Thread.sleep(3);
			r.mouseRelease(InputEvent.BUTTON1_MASK);
			Thread.sleep(5000);
		} catch (InterruptedException e) { e.printStackTrace(); } //Shouldn't happen.

		// Start the game again
		clickStart();
	}
}