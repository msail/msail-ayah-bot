package ayah.bot;

/**
 * A class containing the game instructions, including which shape and
 * color to drag, and which color bin to drag to
 * 
 * @author Gulshan
 *
 */
public class Instructions {

	public static enum Shapes {
		CIRCLE, SQUARE, TRIANGE, STAR;
	}
	
	public static enum Colors {
		BLUE, RED, GREEN, YELLOW, PURPLE, ORANGE;
	}
	
	private Shapes shape;
	private Colors shapeColor;
	private Colors binColor;
	
	public Instructions(Shapes shape, Colors shapeColor, Colors binColor) {
		this.shape = shape;
		this.shapeColor = shapeColor;
		this.binColor = binColor;
	}
	
	public Shapes getShape() {
		return shape;
	}
	
	public Colors getShapeColor() {
		return shapeColor;
	}
	
	public Colors getBinColor() {
		return binColor;
	}
	
}
