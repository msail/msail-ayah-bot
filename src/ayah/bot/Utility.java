package ayah.bot;


import java.awt.Color;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import ayah.bot.Instructions.Colors;

/**
 * This class contains utility methods, such as conversions or helper functions
 * that don't belong in a particular class
 */
public class Utility {

	/**
	 * Given an image of text, this function determines whether that text
	 * contains a word that is a color. If not it returns null
	 * 
	 * TODO: This has an 80% accuracy rate and is slow. Need to optimize
	 * and improve accuracy
	 */
	//TODO: Does this belong in the Bot class? It seems to fall under its scope.
	public static Colors determineColor(Robot r, GameBox gameBox, int startingPoint) {
		
		//NOTE: If debug=true we will NOT read the instructions, but rather, ask for user input.
		boolean debug = true;
		if (debug) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));

			System.out.println("Please enter a color:\n1=blue, 2=red, 3=green, 4=yellow, 5=purple, 6=orange.");
			try {
				int sum = Integer.valueOf(reader.readLine());
				if (sum >= 1)
					return Colors.values()[sum - 1];
			} catch(IOException e) {} //'return null' handles this implicitly.
			return null;
		}
		
		System.out.println("Looking for colors in instructions.");
		
		// Stored cases of instructions (some shapes have two versions)
		BufferedImage purpleImage1, purpleImage2, blueImage, greenImage1, greenImage2;
		BufferedImage redImage, yellowImage1, yellowImage2, orangeImage1, orangeImage2;
		try
		{
			purpleImage1 = ImageIO.read(new File("purple1.png"));
			purpleImage2 = ImageIO.read(new File("purple2.png"));
			blueImage = ImageIO.read(new File("blue.png"));
			greenImage1 = ImageIO.read(new File("green1.png"));
			greenImage2 = ImageIO.read(new File("green2.png"));
			redImage = ImageIO.read(new File("red.png"));
			yellowImage1 = ImageIO.read(new File("yellow1.png"));
			yellowImage2 = ImageIO.read(new File("yellow2.png"));
			orangeImage1 = ImageIO.read(new File("orange1.png"));
			orangeImage2 = ImageIO.read(new File("orange2.png"));
		} catch(IOException e)
		{
			//If we can't open our template files, we can't make the determination,
			//	so just tell it to go to the next instance of the game.
			return null;
		}
		
		int blue = 0, red = 0, yellow = 0, green = 0, purple = 0, orange = 0;
		
		// Check for image at the gameBox.getLeftEdge() and move the starting point to the
		// right if there is no match
		int sum = 0;
		for(int start = startingPoint; start < gameBox.getRightEdge() - 30; start++) {
			
			// If sum is non-zero, we found a color
			if(sum != 0)
				break;			
			
			// Assign values to corresponding to Colors+1
			blue = 1;
			red = 2;
			green = 3;
			yellow = 4;
			purple = 5;
			orange = 6;
			
			// Scan for matching RGB values, zero color variable if not matching
			for(int i = 0; i < 30; i++) {
				Color pixelColor = r.getPixelColor(i + start, gameBox.getTopEdge() + 12);
				//r.mouseMove(i + start, gameBox.getTopEdge() + 12);
				int testRGB = pixelColor.getRGB();
				
				if(testRGB != purpleImage1.getRGB(i, 12) && testRGB != purpleImage2.getRGB(i, 12))
					purple = 0;
				if(testRGB != blueImage.getRGB(i, 12))
					blue = 0;
				if(testRGB != greenImage1.getRGB(i, 12) && testRGB != greenImage2.getRGB(i, 12))
					green = 0;
				if(testRGB != redImage.getRGB(i, 12))
					red = 0;
				if(testRGB != yellowImage1.getRGB(i, 12) && testRGB != yellowImage2.getRGB(i, 12))	
					yellow = 0;
				if(testRGB != orangeImage1.getRGB(i, 12) && testRGB != orangeImage2.getRGB(i, 12))	
					orange = 0;
							
				sum = purple + blue + green + red + yellow + orange;
				if (sum == 0) {
					break;
				}
			}
			
			// If sum is non-zero, we found a color
			if(sum != 0)
				break;
		}

		// Only one color should be left, return that color. Else return null
		if(sum >= 1) {
			return Colors.values()[sum - 1];
		} else {
			return null;
		}
	}
	
	/************************************************************************/

	//TODO: These next two methods seem very messy, we should be able to identify points
	//		on our bins that have the same exact colors as the objects.
	
	
	//Converts the Colors enum values to RGB values for the shapes
	public static Color convertShapeColorToRGB(Colors shapeColor) {
		
		Color c = null;
		switch(shapeColor) {
		case RED:
			c = new Color(255, 0, 0);
			break;
		case BLUE:
			c = new Color(38, 110, 255);
			break;
		case GREEN:
			c = new Color(15, 156, 0);
			break;
		case YELLOW:
			c = new Color(255, 255, 0);
			break;
		case PURPLE:
			c = new Color(219, 20, 255);
			break;
		case ORANGE:
			c = new Color(255, 115, 0);
			break;
		}
		
		return c;
	}
	
	/************************************************************************/

	//Converts the Colors enum values to RGB values for the bins
	public static Color convertBinColorToRGB(Colors binColor) {

		Color c = null;
		switch(binColor) {
		case RED:
			c = new Color(254, 0, 0);
			break;
		case BLUE:
			c = new Color(36, 111, 255);
			break;
		case GREEN:
			c = new Color(15, 156, 0);
			break;
		case YELLOW:
			c = new Color(255, 255, 0);
			break;
		case PURPLE:
			c = new Color(218, 20, 255);
			break;
		case ORANGE:
			c = new Color(255, 119, 1);
			break;
		}
		
		return c;
	}

}
