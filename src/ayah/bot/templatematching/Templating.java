package ayah.bot.templatematching;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;

public class Templating extends Tracker {
	private int templateWidth, templateHeight;
	private Item type;
	private int[] TemplateRGBData; // template raster

	public Templating(BufferedImage template, Item type) {
		this.templateWidth = template.getWidth();
		this.templateHeight = template.getHeight();
		this.type = type;
		// TemplateRGBData = ((DataBufferInt)(template.getRaster().getDataBuffer())).getData();

		TemplateRGBData = new int[templateHeight * templateWidth];
		for (int y = 0; y < templateHeight; y++) {
			for (int x = 0; x < templateWidth; x++) {
				//TODO: Figure out why in BLAZES I'd have done this this way?
				//Shit. I did this to prevent the DataBufferByte crap.
				TemplateRGBData[x + y * templateWidth] = template.getRGB(x, y);
			}
		}
	}

	// Where the algorithm has to run
	// doesn't do anything with theta, just threw this in to make it work
	public ArrayList<Item> findItems(BufferedImage Img) {
		int imgHeight = Img.getHeight();
		int imgWidth = Img.getWidth();

		ArrayList<Item> Items = new ArrayList<Item>();

		int[] RGBData = ((DataBufferInt) (Img.getRaster().getDataBuffer())).getData();
		double[][] Error = new double[imgWidth - templateWidth][imgHeight - templateHeight];
		double AvgError = 0;
		int cntr = 0; // how many error values have we computed
		double curError, minErrorRow;
		int skipDistX = 1;
		int skipDistY = 1;
		// step through positions in image but stop short to avoid moving beyond image
		// find template response for each pixel
		for (int y = 0; y < (imgHeight - templateHeight); y += skipDistY) //Rows in img
		{
			minErrorRow = Double.MAX_VALUE;
			for (int x = 0; x < (imgWidth - templateWidth); x += skipDistX) // columns in img
			{
				curError = Response(RGBData, new int[] { x, y }, imgHeight, imgWidth);

				// capture the minimum error in the row, for skipping rows
				if (curError < minErrorRow) {
					minErrorRow = curError;
				}

				// compute a skip distance depending on the error
				// large error -> skip greater distance not a ball
				AvgError = ((cntr) * AvgError + curError) / (cntr + 1);

				skipDistX = (int) ((curError / AvgError) * templateWidth / 5.0); // 5 is a magic number
				// can't have skip values less than 1
				if (skipDistX < 1) {
					skipDistX = 1;
				}

				// System.out.println("Avg Error: " + AvgError + " CurError: " + curError + " skipDistX: " + skipDistX);
				// set error for the sections we are skipping over
				for (int i = 0; i < skipDistX; i++) {
					if ((x + i) >= (imgWidth - templateWidth)) {
						break;
					}

					Error[x + i][y] = curError;
				}
				cntr++;
			}
			skipDistY = (int) ((minErrorRow / AvgError) * templateHeight / 5.0);
			if (skipDistY < 1) {
				skipDistY = 1; // must skip atleast 1 row, or else
			}
			// now use the skip Y distance to set values along the row to our
			// approximate values

			for (int j = 0; j < skipDistY; j++) { // row
				if ((y + j) >= Error[0].length) {
					break;
				}
				for (int i = 0; i < Error.length; i++) { // column
					Error[i][y + j] = Error[i][y]; // set error values down the columns
				}
			}
		} // Outer for loop

		ArrayList<int[]> Cords = Suppression.nonMinima(Error, 5, AvgError * 0.2);

		// make sure we haven't already added one close to this location
		boolean flag = false;
		int[] candidate, previous;
		for (int i = 0; i < Cords.size(); i++) {
			flag = false;
			candidate = Cords.get(i);
			for (int k = 0; k < i; k++) {
				previous = Cords.get(k);
				// know that balls can't overlap
				if ((Math.abs(candidate[0] - previous[0]) < (templateWidth / 2))
						&& Math.abs(candidate[1] - previous[1]) < (templateHeight / 2)) {
					// System.out.println("Overlap detected");
					flag = true;
				}
			}
			if (!flag)
				Items.add(type.getThisItem(new double[] {
						candidate[0] + templateWidth / 2,
						candidate[1] + templateHeight / 2 }));
		}
		// 0.2 is a magic number
		// LocalMinima(Error, AvgError * .15, Items);

		return Items;
	}

	private double Response(int[] ImgRGBData, int[] pixel, int imgHeight,
			int imgWidth) {
		int ir, ig, ib, tr, tg, tb, rgb; // image and template rgb values

		double error = 0;
		// increment is a down-sampling number, its magic
		for (int j = 0; j < templateHeight; j += 3) // rows in template
		{
			for (int i = 0; i < templateWidth; i += 3) // columns in template
			{
				rgb = ImgRGBData[(pixel[0] + pixel[1] * imgWidth) + (i + j * imgWidth)];
				// Scan through image and retrieve color channels
				ir = ((rgb >> 16) & 0xFF);
				ig = ((rgb >> 8) & 0xFF);
				ib = ((rgb) & 0xFF);
				rgb = TemplateRGBData[i + j * templateWidth];
				// Scan for (int i = 0; i)through image and retrieve color channels
				tr = ((rgb >> 16) & 0xFF);
				tg = ((rgb >> 8) & 0xFF);
				tb = ((rgb) & 0xFF);

				// calculate squared euclidian eror
				error += (ir - tr) * (ir - tr) + (ig - tg) * (ig - tg)
						+ (ib - tb) * (ib - tb);
			}
		}
		return error;
	}

	/*
	 * private void LocalMinima(double[][] Error, double threshold,
	 * ArrayList<Item> Items) { int[] minima = null; int widthLimit,
	 * heightLimit, widthInit, heightInit;
	 * 
	 * do { minima = GlobalMinima(Error, threshold); if (minima != null) {
	 * widthInit = ((minima[0] - templateWidth / 2 < 0) ? 0 : minima[0] -
	 * templateWidth / 2); heightInit = ((minima[1] - templateHeight / 2 < 0) ?
	 * 0 : minima[1] - templateHeight / 2); widthLimit = ((minima[0] +
	 * templateWidth / 2 > Error.length) ? Error.length : minima[0] +
	 * templateWidth / 2); heightLimit = ((minima[1] + templateHeight / 2 >
	 * Error[0].length) ? Error[0].length : minima[1] + templateHeight / 2);
	 * Items.add(type.getThisItem(new double[]{minima[0] + templateWidth / 2,
	 * minima[1] + templateHeight / 2})); for (int i = widthInit; i <
	 * widthLimit; i++) { for (int j = heightInit; j < heightLimit; j++) {
	 * Error[i][j] = Double.MAX_VALUE; } }
	 * 
	 * } else { return; } } while (true); }
	 */

	// assumes that minima will be seperated by template width to accelerate the
	// search
	/*
	 * private int[] GlobalMinima(double[][] Error, double threshold) { double
	 * minE = Double.MAX_VALUE; int[] loc = new int[2]; for (int y = 0; y <
	 * Error[0].length; y += templateHeight / 4) { for (int x = 0; x <
	 * Error.length; x += templateWidth / 4) { // get global maxima if
	 * (Error[x][y] < minE) { minE = Error[x][y]; loc[0] = x; loc[1] = y; } } }
	 * loc = GradientDescent(Error, loc);
	 * 
	 * if (minE < threshold) { return loc; } else { return null; } }
	 */

	/*private final static int gradientLimit = 2;

	private int[] GradientDescent(double[][] Error, int[] loc) {
		boolean arrived = true;
		int lowest[] = loc;
		int xInit, yInit, xLimit, yLimit;
		do {
			xInit = (lowest[0] - gradientLimit < 0 ? 0 : lowest[0]
					- gradientLimit);
			yInit = ((lowest[1] - gradientLimit < 0) ? 0
					: (lowest[1] - gradientLimit));
			xLimit = (lowest[0] + gradientLimit > Error.length ? Error.length
					: lowest[0] + gradientLimit);
			yLimit = xLimit = (lowest[1] + gradientLimit > Error[0].length ? Error[0].length
					: lowest[1] + gradientLimit);
			arrived = true;
			for (int i = xInit; i < xLimit; i++) {
				for (int j = yInit; j < yLimit; j++) {
					if (Error[i][j] < Error[lowest[0]][lowest[1]]) {
						lowest[0] = i;
						lowest[1] = j;
						arrived = false;
					}
				}
			}
		} while (!arrived);

		return lowest;
	}*/
}

/*
 * // lets try to grab local minima double threshold = 10000; // throw out all
 * errors above this value, not a good enough item for (int y = 0; y <
 * Error[0].length; y++) { boolean increasingX, increasingY; // handle
 * increasing or decreasing for begining of row if ((Error[1][y] - Error[0][y])
 * > 0) { increasingX = true; } else { increasingX = false; }
 * 
 * // handle increasing or decreasing for column for (int x = 0; x <
 * Error.length; x++) { // if we changed sign if (!(((Error[x+1][y] -
 * Error[x][y]) > 0) == (increasing))) { double delta = Math.abs(Error[x-2][y] -
 * Error[x+2][y]); if () {
 * 
 * } } } }
 */
