
package ayah.bot.templatematching;


public abstract class Item
{
    protected double[] imgPos;
    protected double[] worldPos;
    protected double rotation;

    public abstract Item getThisItem(double[] pos);

    public double[] getImgPos() {
        return imgPos;
    }

	public void setWorldPos(double[] wp) {
		worldPos = wp;
    }

	public double[] getWorldPos() {
		return worldPos;
    }

	public double getRotatation() {
		return rotation;
    }

	public void setRotation(double d) {
		rotation = d;
    }

}
