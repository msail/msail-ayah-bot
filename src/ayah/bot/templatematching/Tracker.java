
package ayah.bot.templatematching;

import java.awt.image.BufferedImage;
import java.util.ArrayList;


public abstract class Tracker
{
	/**
	 * Finds the image specified by {@code Img}.
	 * 
	 * @param Img
	 * @return
	 */
    public abstract ArrayList<Item> findItems(BufferedImage Img);
}
