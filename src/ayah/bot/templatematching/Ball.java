

package ayah.bot.templatematching;


public class Ball extends Item {

    public Ball(double[] pos) {
        this.imgPos = pos;
    }

	public Item getThisItem(double[] pos) {
        return new Ball(pos);
    }
}
