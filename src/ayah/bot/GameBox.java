package ayah.bot;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;

//NOTE: We run on the assumption that the browser window containing the game
//		is at the top left corner of your screen.
public class GameBox {
	
	
	//Left various edges/boundaries of the game box.
	private int leftEdge = -1;
	private int rightEdge = -1;
	private int topEdge = -1;
	private int bottomEdge = -1;
	
	
	//Offset from left to start button
	public static final int START_BUTTON_X_OFFSET = 173;
	//Offset from topEdge to start button
	public static final int START_BUTTON_Y_OFFSET = 87;
	
	
	///Offset from topEdge of each bin's Y coordinate
	public static final int BIN_Y_OFFSET = 110;
	//Offset from leftEdge of this bin's X coordinate
	public static final int BIN_ONE_X_OFFSET = 56, BIN_TWO_X_OFFSET = 159, BIN_THREE_X_OFFSET = 264;
	//Offset from topEdge to the top of the bins 
	public static final int BIN_TOP_OFFSET = 92;
	
	
	//Height of instructions text
	public static final int TEXT_HEIGHT = 26;
	
	
	//Background color of the game
	public static final Color BACKGROUND_COLOR = new Color(38, 110, 255);
	
	/************************************************************************/

	public GameBox() {
		calibrate();
	}
	
	/************************************************************************/

	//Find the dimensions of the Game Box
	private void calibrate() {
		//NOTE: If debug=true, we will NOT calibrate, but rather, assume the following set of coordinates.
		boolean debug = false;
		if(debug == true) {
			leftEdge = 16;
			rightEdge = 370;
			topEdge = 423;
			bottomEdge = 555;
			return;
		}
		
		Robot r = null;
		try {
			r = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
			System.exit(1); //Would indicate something exceedingly bad.
		}
		
		System.out.println("Starting Calibration.");
		
		// Get the screen size
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		// Scan the page from top to bottom to find the top of Game Box
		// Start scanning at a 'good minimum guess' of 250.
		for(int i = 250; i < dim.height; i++) {
			// If we find the background, save the position
			if (r.getPixelColor(50, i).equals(BACKGROUND_COLOR)) {
				topEdge = i;
				System.out.println("Found top edge: " + topEdge);
				break;
			}
		}
		
		// If we didn't find the box
		if (topEdge == -1) {
			System.out.println("Game box could not be found. Reposition your window and try again.");
			System.exit(-1);
		}
		
		// Start at the top value and scan to the left until a color that is 
		// not the background color is found
		for(int i = 50; i > 0; i--) {
			// If we find the non-background, save the position
			if (!r.getPixelColor(i, topEdge).equals(BACKGROUND_COLOR)) {
				leftEdge = i;
				System.out.println("Found left edge: " + leftEdge);
				break;
			}
		}
		
		// Start at the top value and scan to the right until a color that is 
		// not the background color is found
		// Start at a 'good minimum guess' based on previously known dimensions.
		for(int i = leftEdge+325; i < dim.width; i++) {
			// If we find the non-background, save the position
			if (!r.getPixelColor(i, topEdge).equals(BACKGROUND_COLOR)) {
				rightEdge = i;
				System.out.println("Found right edge: " + rightEdge);
				break;
			}
		}
		
		// Start at the top value and scan to the bottom until a color that is 
		// not the background color is found
		// Start at a 'good minimum guess' based on previously known dimensions.
		for(int i = topEdge+100; i < dim.height; i++) {
			// If we find the non-background, save the position
			if (!r.getPixelColor(50, i).equals(BACKGROUND_COLOR)) {
				bottomEdge = i;
				System.out.println("Found bottom edge: " + bottomEdge);
				break;
			}
		}
	}
	
	/************************************************************************/

	public int getLeftEdge() {
		return leftEdge;
	}
	
	/************************************************************************/

	public int getRightEdge() {
		return rightEdge;
	}
	
	/************************************************************************/

	public int getBottomEdge() {
		return bottomEdge;
	}
	
	/************************************************************************/

	public int getTopEdge() {
		return topEdge;
	}
}
